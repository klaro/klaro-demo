<?php

namespace KlaroCPQBundle\NodeProcessor;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ConfigurationProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;

class EmptySectionRemovalNodeProcessor implements NodeProcessorInterface
{
    public function setContext(
        ConfigurationNode $rootProduct,
        ConfigurationNode $product,
        ConfigurationProcessorInterface $configurationProcessor,
        ConfigurationContextProviderInterface $contextProvider = null
    ) {

    }

    public function process(ConfigurationNode $node)
    {
        foreach ($node->getSections() as $section) {
            if (!$section->hasItem() && !$section->hasSections()) {
                $section->getParent()->removeSection($section->getId());
            } else {
                $this->process($section);
            }
        }
    }
}
