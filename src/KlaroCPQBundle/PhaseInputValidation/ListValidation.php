<?php

namespace KlaroCPQBundle\PhaseInputValidation;

use Particle\Filter\Filter;
use Particle\Validator\Validator;
use Klaro\Component\Validation\Failure;
use Klaro\Component\Validation\FormPhaseDataSanitizerInterface;
use Klaro\Component\Validation\FormPhaseDataValidatorInterface;
use Klaro\Component\Validation\ValidationResult;

class ListValidation implements FormPhaseDataSanitizerInterface, FormPhaseDataValidatorInterface {
    /**
     * {@inheritdoc}
     */
    public function sanitize(array $values)
    {
        $filter = new Filter;

        $filter->value('Items')->defaults([])->each(function(Filter $innerFilter) {
            $innerFilter->value('code')->string()->defaults('');
            $innerFilter->value('name')->string()->defaults('');
            $innerFilter->value('quantity')->float()->defaults(1);
            $innerFilter->value('cost')->float()->defaults(0);
            $innerFilter->value('price')->float()->defaults(0);
            $innerFilter->value('section')->string()->defaults('Products');
        });

        return $filter->filter($values);
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $values, ValidationResult $result)
    {
        $validator = new Validator();

        $validator->optional('Items')->each(function(Validator $innerValidator) {
            $innerValidator->optional('code')->string();
            $innerValidator->optional('name')->string();
            $innerValidator->optional('quantity')->float();
            $innerValidator->optional('cost')->float();
            $innerValidator->optional('price')->float();
            $innerValidator->optional('section')->string()->inArray(['Products', 'Services', 'Extra']);
        });

        $validationResult = $validator->validate($values);

        if (!$validationResult->isValid()) {
            foreach ($validationResult->getMessages() as $field => $messageData) {
                foreach ($messageData as $reason => $message) {
                    $result->addValidationIssue(new Failure($field, $reason, $message, null));
                }
            }
        }

        $result->addValues($validationResult->getValues());
    }
}
