<?php

namespace KlaroCPQBundle\PhaseInputValidation;

use Particle\Filter\Filter;
use Particle\Validator\Validator;
use Klaro\Component\Validation\Failure;
use Klaro\Component\Validation\FormPhaseDataSanitizerInterface;
use Klaro\Component\Validation\FormPhaseDataValidatorInterface;
use Klaro\Component\Validation\ValidationResult;

class GeneralValidation implements FormPhaseDataSanitizerInterface, FormPhaseDataValidatorInterface {
    /**
     * {@inheritdoc}
     */
    public function sanitize(array $values) {
        $filter = new Filter;

        $filter->value('CustomerName')->string()->defaults('');
        $filter->value('ProjectName')->string()->defaults('');
        $filter->value('Office')->string()->defaults('New York');

        $filter->value('DeliveryAddress')->string()->defaults('Kaisaniemenkatu 4 A (7.krs)
00100 Helsinki, Finland');

        $filter->value('QuotationDate')->string()->defaults(date('d.m.Y'));
        $filter->value('ValidityDate')->string()->defaults(date('d.m.Y', strtotime('+30 days')));
        $filter->value('StartDate')->string()->defaults(date('d.m.Y', strtotime('+90 days')));

        $filter->value('AdditionalInfo')->string()->defaults('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

        return $filter->filter($values);
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $values, ValidationResult $result)
    {
        $validator = new Validator();

        $validator->required('CustomerName')->string();
        $validator->required('ProjectName')->string();
        $validator->required('Office')->string()->inArray(['New York', 'Helsinki', 'Singapore']);

        $validator->required('DeliveryAddress')->string();

        $validator->required('QuotationDate')->string();
        $validator->required('ValidityDate')->string();
        $validator->required('StartDate')->string();

        $validator->required('AdditionalInfo')->string();

        $validationResult = $validator->validate($values);

        if (!$validationResult->isValid()) {
            foreach ($validationResult->getMessages() as $field => $messageData) {
                foreach ($messageData as $reason => $message) {
                    $result->addValidationIssue(new Failure($field, $reason, $message, null));
                }
            }
        }

        $result->addValues($validationResult->getValues());
    }
}
