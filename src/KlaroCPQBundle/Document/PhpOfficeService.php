<?php

namespace KlaroCPQBundle\Document;

use PhpOffice\PhpWord\PhpWord;

class PhpOfficeService
{
    /* var PhpWord */
    public $phpWord;

    public function __construct()
    {
        $this->phpWord = new PhpWord();
    }
}
