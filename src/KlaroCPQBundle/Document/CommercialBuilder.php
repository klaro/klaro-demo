<?php

namespace KlaroCPQBundle\Document;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\DocumentManagerBundle\Entity\DocumentTemplateInterface;
use Klaro\DocumentManagerBundle\Library\DocumentManager;
use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;
use Klaro\QuotationBundle\Document\OutputDocumentManager;
use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\QuotationBundle\Document\Word\WordOutputDocument;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;

use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\Element\Table;
use PhpOffice\PhpWord\SimpleType\TblWidth;
use \PhpOffice\PhpWord\SimpleType\Jc;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

class CommercialBuilder  implements DocumentBuilderInterface
{
    /** @var OutputDocumentManager */
    protected $context;

    /** @var DocumentManager */
    protected $documentManager;

    /** @var QuotationFacade */
    protected $quotationFacade;

    protected $mappingErrors = [];

    /**
     * @param QuotationFacade $quotationFacade
     * @param DocumentManager $documentManager
     */
    public function __construct(QuotationFacade $quotationFacade, DocumentManager $documentManager)
    {
        $this->quotationFacade = $quotationFacade;
        $this->documentManager = $documentManager;
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(OutputDocumentManager $context)
    {
        $this->context = $context;
    }

    /**
     * {@inheritDoc}
     */
    public function generate(QuotationRevisionInterface $revision, DocumentDefinitionInterface $definition)
    {
        ini_set('memory_limit', '4G');
        ini_set('max_execution_time', 120);

        $offeringSummary = $this->quotationFacade->getConfigurationForRevision($revision, ConfiguratorManagerInterface::FETCH_FROM_SOURCE);

        $temp_file = tempnam(sys_get_temp_dir(), 'KLAROCPQ');

        /** @var DocumentTemplateInterface $template */
        $template = $this->documentManager->findLastTemplateByGroup($definition->getSource());

        $file = tmpfile();
        fwrite($file, $template->getFileContent());
        rename(stream_get_meta_data($file)['uri'], stream_get_meta_data($file)['uri'] .= '.docx');
        $templatePath = stream_get_meta_data($file)['uri'] . '.docx';

        $templateProcessor = new TemplateProcessor($templatePath);
        $this->fullFillDocument($templateProcessor, $this->quotationFacade->getFormDataForRevision($revision), $offeringSummary);
        $templateProcessor->saveAs($temp_file);

        return WordOutputDocument::create()
            ->setServerPath($temp_file)
            ->setFileName($definition->getTitle())
            ->setTitle($definition->getTitle())
            ->setTemporary(true)
            ->setSaved(true);
    }

    protected function fullFillDocument(TemplateProcessor $templateProcessor, ProductLineDataNode $formData,
                                        ConfigurationNode $offeringSummary)
    {
        $offeringSummaryTable = $this->buildPricesTable($offeringSummary);
        $templateProcessor->setComplexBlock('offeringSummary', $offeringSummaryTable);
        $templateProcessor->setValue('offeringPrice', ceil($offeringSummary->getTotalSalesPriceWithTax()));

        $this->autoMapVariables($templateProcessor, $formData);
        $this->autoMapSymfonyVariables($templateProcessor, $formData);

        foreach ($this->mappingErrors as $key => $value) {
            $this->mappingErrors[$key] = htmlspecialchars($this->mappingErrors[$key]);
        }
        $templateProcessor->setValue('debug', implode("<w:br/>", $this->mappingErrors));
    }

    private function buildPricesTable(ConfigurationNode $startSection, int $maxDepth = 5, int $currentDepth = 1,
                                      Table $table = null): Table
    {
        $tabulation = 150;
        $cellRightAlign = ['alignment' => Jc::END];
        $titleFont = ['bold' => true];
        $bottomLine = ['borderBottomSize' => 1];
        $topLine = ['borderTopSize' => 1];

        if ($currentDepth === 1) {
            $table = new Table(array(
                'unit' => TblWidth::PERCENT,
                'width' => 100 * 50,
            ));

            $table->addRow();
            $table->addCell(null, ['gridSpan' => $maxDepth] + $bottomLine);
            $table->addCell($tabulation, $bottomLine)->addText('Unit');
            $table->addCell($tabulation, $bottomLine)->addText('Quantity');
            $table->addCell($tabulation, $bottomLine)->addText('Total');

            $table->addRow();
            $table->addCell(null, ['gridSpan' => $maxDepth] + $bottomLine)->addText($startSection->getTitle(), $titleFont);
            $table->addCell($tabulation, $bottomLine)->addTextRun($cellRightAlign); // no unit price on top level
            $table->addCell($tabulation, $bottomLine)->addTextRun($cellRightAlign); // no quantity on top level
            $table->addCell($tabulation, $bottomLine)->addTextRun($cellRightAlign)->addText(ceil($startSection->getTotalSalesPriceWithTax()), $titleFont);
        } else {
            $table->addRow();
            for ($i = 1; $i < $currentDepth; $i++) {
                $table->addCell($tabulation);
            }
            $table->addCell(null, ['gridSpan' => $maxDepth - $currentDepth + 1])->addText($startSection->getTitle());
            $table->addCell($tabulation)->addTextRun($cellRightAlign)->addText($startSection->getInputData('Quantity') ?
                $startSection->getInputData('SalesPrice') : '');
            $table->addCell($tabulation)->addTextRun($cellRightAlign)->addText($startSection->getInputData('Quantity') ?: '');
            $table->addCell($tabulation)->addTextRun($cellRightAlign)->addText(ceil($startSection->getTotalSalesPriceWithTax()));
        }

        if ($maxDepth === $currentDepth) {
            return $table;
        }

        foreach ($startSection->getSections() as $section) {
            $this->buildPricesTable($section, $maxDepth, $currentDepth + 1, $table);
        }

        if ($currentDepth === 1) {
            $table->addRow();
            $table->addCell(null, ['gridSpan' => $maxDepth + 3] + $topLine);
        }

        return $table;
    }

    private function autoMapVariables(TemplateProcessor $templateProcessor, ProductLineDataNode $formData)
    {
        $pattern = '/^([a-zA-Z_0-9]+)(?:\/([a-zA-Z_0-9]+))?\:([a-zA-Z_0-9]+)$/';
        $variables = $templateProcessor->getVariables();

        foreach ($variables as $variable) {
            if (!preg_match($pattern, $variable, $matches)) {
                $this->mappingErrors[] = sprintf('Skipping "%s": doesn\'t match "<phase>[/subphase]:field" definition', $variable);
                continue;
            }

            if (!array_key_exists($matches[1], $formData->getChildren())) {
                $this->mappingErrors[] = sprintf('Error while mapping "%s": phase "%s" doesn\'t exist', $variable, $matches[1]);
                continue;
            }
            $phase = $formData->getChild($matches[1]);

            if ($matches[2]) {
                if (!$phase->hasChildren()) {
                    $this->mappingErrors[] = sprintf('Error while mapping "%s": phase "%s" has no subphases', $variable, $matches[1]);
                    continue;
                }
                if (!array_key_exists($matches[2], $phase->getChildren())) {
                    $this->mappingErrors[] = sprintf('Error while mapping "%s": subphase "%s" doesn\'t exist', $variable, $matches[2]);
                    continue;
                }
                $phase = $phase->getChild($matches[2]);
            }

            if ($phase->hasChildren()) {
                $this->mappingErrors[] = sprintf('Error while mapping "%s": "%s" is a group of phases', $variable, $matches[1]);
                continue;
            }

            if (($value = $phase->getData($matches[3])) === null) {
                $this->mappingErrors[] = sprintf('Error while mapping "%s": field "%s" doesn\'t exist', $variable, $matches[3]);
                continue;
            }

            $templateProcessor->setValue($variable, $value);
        }

    }

    private function autoMapSymfonyVariables(TemplateProcessor $templateProcessor, ProductLineDataNode $formData)
    {
        $pattern = '/^:(.+)/';
        $variables = $templateProcessor->getVariables();

        $expressionLanguage = new ExpressionLanguage();

        foreach ($variables as $variable) {
            if (!preg_match($pattern, $variable, $matches)) {
                $this->mappingErrors[] = sprintf('Skipping "%s": doesn\'t match ":symfonyExpression" definition', $variable);
                continue;
            }

            try {
                $value = $expressionLanguage->evaluate($matches[1], ['formData' => $formData]);
                $templateProcessor->setValue($variable, $value);
            } catch (\Exception $e) {
                $this->mappingErrors[] = sprintf('Error while mapping "%s": "%s"', $variable, $e->getMessage());
            }
        }
    }
}
