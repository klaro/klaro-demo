<?php

namespace KlaroCPQBundle\Controller;

use Klaro\QuotationBundle\Controller\QuotationController as BaseQuotationController;;

class QuotationController extends BaseQuotationController
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function mainAction() {
        return $this->redirect($this->generateUrl('klaro_quotation_index'));
    }
}
