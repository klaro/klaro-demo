<?php

namespace KlaroCPQBundle\CurrencyConverter;

use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverterInterface;
use Klaro\QuotationBundle\CurrencyConverter\FormDataAwareCurrencyConverter;

class CurrencyConverter  implements CurrencyConverterInterface, FormDataAwareCurrencyConverter
{
    /**
     * @return string
     */
    public function getDefaultCurrency()
    {
        // TODO: Implement getDefaultCurrency() method.

        return 'EUR';
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        // TODO: Implement getCurrency() method.

        return 'EUR';
    }

    /**
     * @param $amount
     * @return float
     */
    public function toCurrency($amount)
    {
        // TODO: Implement toCurrency() method.

        return $amount;
    }

    /**
     * @param $amount
     * @return float
     */
    public function fromCurrency($amount)
    {
        // TODO: Implement fromCurrency() method.

        return $amount;
    }

    public function setFormData(ProductLineDataNode $formData)
    {
        // TODO: Implement setFormData() method.
    }
}