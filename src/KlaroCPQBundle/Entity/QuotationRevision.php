<?php

namespace KlaroCPQBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Klaro\QuotationExtraBundle\Model\Revision;

/**
 * @ORM\Entity
 * @ORM\Table(name="cpq_quotation_revision")
 */
class QuotationRevision extends Revision
{
    /**
     * @ORM\OneToMany(targetEntity="Klaro\Component\Common\Model\FormModelInterface", mappedBy="revision", cascade={"persist"})
     */
    protected $models;
}
