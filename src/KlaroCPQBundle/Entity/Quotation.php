<?php

namespace KlaroCPQBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\AssociationOverride;
use Doctrine\ORM\Mapping\AssociationOverrides;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Klaro\QuotationExtraBundle\Model\Quotation as BaseQuotation;

/**
 * @ORM\Entity
 * @ORM\Table(name="cpq_quotation")
 * @AssociationOverrides({
 *      @AssociationOverride(name="linkedUsers",
 *          joinTable=@JoinTable(
 *              name="cpq_quotation_user_link",
 *              joinColumns=@JoinColumn(name="quotation_id", referencedColumnName="id", onDelete="cascade"),
 *              inverseJoinColumns=@JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")
 *          )
 *      )
 * })
 */
class Quotation extends BaseQuotation
{
    /**
     * @ORM\OneToMany(targetEntity="Klaro\Component\Common\Model\QuotationRevisionInterface", mappedBy="quotation", indexBy="revisionId", cascade={"persist"})
     */
    protected $revisions;
}
