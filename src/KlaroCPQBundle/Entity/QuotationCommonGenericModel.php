<?php

namespace KlaroCPQBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Klaro\QuotationExtraBundle\Model\FormModel;

/**
 * @ORM\Entity
 * @ORM\Table(name="cpq_generic_model")
 */
class QuotationCommonGenericModel extends FormModel
{

}