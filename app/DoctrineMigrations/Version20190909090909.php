<?php

declare(strict_types=1);

namespace KlaroCPQBundle\Migrations;

use Klaro\DocumentManagerBundle\Migrations\BaseMigration;

require_once __DIR__ . '/../../vendor/klaro/document-manager-bundle/DocumentManagerBundle/Migration/BaseMigration.php';

final class Version20190909090909 extends BaseMigration
{
    // define document groups
    const DOCUMENTS = [
        [
            'group_id' => 1,
            'identifier' => 'klarocpq_commercial_quotation',
            'name' => 'Commercial Quotation Template',
            'file_name' => 'klarocpq_commercial_quotation_#revision#.docx',
            // define init file (optional)
            'file' => __DIR__ . '/../../src/KlaroCPQBundle/Resources/data/documents/commercial.docx',
        ],
    ];
}
