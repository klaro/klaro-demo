<?php

declare(strict_types=1);

namespace KlaroCPQBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190423090047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE cpq_quotation_revision (id VARCHAR(20) NOT NULL, quotation_id INT DEFAULT NULL, user_id INT DEFAULT NULL, revision_id INT NOT NULL, qualifiers LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', identifier VARCHAR(100) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, status VARCHAR(50) DEFAULT NULL, order_status VARCHAR(50) DEFAULT NULL, metadatas LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', sales_structure LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', product_items LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', phases LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', product_line VARCHAR(255) DEFAULT NULL, product_line_version VARCHAR(50) DEFAULT NULL, configurator VARCHAR(255) DEFAULT NULL, configurator_version VARCHAR(50) DEFAULT NULL, offering_summary LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:object)\', phase_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_B65A9F3CB4EA4E60 (quotation_id), INDEX IDX_B65A9F3CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpq_quotation (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, latest_revision_id VARCHAR(20) DEFAULT NULL, identifier VARCHAR(100) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, quotation_type VARCHAR(100) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_4EF7B8077E3C61F9 (owner_id), INDEX IDX_4EF7B807C6556E3E (latest_revision_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpq_quotation_user_link (quotation_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_F14B1EAEB4EA4E60 (quotation_id), INDEX IDX_F14B1EAEA76ED395 (user_id), PRIMARY KEY(quotation_id, user_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_957A647992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_957A6479A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_957A6479C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cpq_generic_model (id VARCHAR(135) NOT NULL, revision_id VARCHAR(20) DEFAULT NULL, phase_id VARCHAR(100) DEFAULT NULL, sub_id INT NOT NULL, locked TINYINT(1) DEFAULT NULL, form_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_4913BC481DFA7C8F (revision_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE klaro_configurator_versioned_config (name VARCHAR(255) NOT NULL, version VARCHAR(50) NOT NULL, updated_at DATETIME NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(name, version)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE klaro_product_manager_versioned_config (name VARCHAR(255) NOT NULL, version VARCHAR(50) NOT NULL, updated_at DATETIME NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(name, version)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cpq_quotation_revision ADD CONSTRAINT FK_B65A9F3CB4EA4E60 FOREIGN KEY (quotation_id) REFERENCES cpq_quotation (id)');
        $this->addSql('ALTER TABLE cpq_quotation_revision ADD CONSTRAINT FK_B65A9F3CA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE cpq_quotation ADD CONSTRAINT FK_4EF7B8077E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE cpq_quotation ADD CONSTRAINT FK_4EF7B807C6556E3E FOREIGN KEY (latest_revision_id) REFERENCES cpq_quotation_revision (id)');
        $this->addSql('ALTER TABLE cpq_quotation_user_link ADD CONSTRAINT FK_F14B1EAEB4EA4E60 FOREIGN KEY (quotation_id) REFERENCES cpq_quotation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cpq_quotation_user_link ADD CONSTRAINT FK_F14B1EAEA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cpq_generic_model ADD CONSTRAINT FK_4913BC481DFA7C8F FOREIGN KEY (revision_id) REFERENCES cpq_quotation_revision (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cpq_quotation DROP FOREIGN KEY FK_4EF7B807C6556E3E');
        $this->addSql('ALTER TABLE cpq_generic_model DROP FOREIGN KEY FK_4913BC481DFA7C8F');
        $this->addSql('ALTER TABLE cpq_quotation_revision DROP FOREIGN KEY FK_B65A9F3CB4EA4E60');
        $this->addSql('ALTER TABLE cpq_quotation_user_link DROP FOREIGN KEY FK_F14B1EAEB4EA4E60');
        $this->addSql('ALTER TABLE cpq_quotation_revision DROP FOREIGN KEY FK_B65A9F3CA76ED395');
        $this->addSql('ALTER TABLE cpq_quotation DROP FOREIGN KEY FK_4EF7B8077E3C61F9');
        $this->addSql('ALTER TABLE cpq_quotation_user_link DROP FOREIGN KEY FK_F14B1EAEA76ED395');
        $this->addSql('DROP TABLE cpq_quotation_revision');
        $this->addSql('DROP TABLE cpq_quotation');
        $this->addSql('DROP TABLE cpq_quotation_user_link');
        $this->addSql('DROP TABLE fos_user');
        $this->addSql('DROP TABLE cpq_generic_model');
        $this->addSql('DROP TABLE klaro_configurator_versioned_config');
        $this->addSql('DROP TABLE klaro_product_manager_versioned_config');
    }
}
