KlaroCPQ Demo Project
==========================

# Installation

## Getting sources

Use `git clone git@gitlab.com:klaro/klaro-demo.git` to checkout project into `klaro-demo` directory.

## Creating database tables

Use `make db` to create required database tables.

## Assets

Use `make prod` to install for production environment (and `make assets-prod` for assets).

## Creating users

Do the following to create admin user:

```
php bin/console fos:user:create admin admin@cpq3-demo.com test
php bin/console fos:user:promote admin ROLE_ADMIN
```

# Documentation

The documentation can be found in [doc.klarocpq.com](https://doc.klarocpq.com) website.
