COMPOSER         = composer install || php composer.phar install
COMPOSER_UPDATE  = composer update  || php composer.phar update
BOWER            = bower install
CONSOLE          = php bin/console
CC               = $(CONSOLE) cache:clear
ASSETS           = $(CONSOLE) assets:install --symlink web
ASSETIC          = $(CONSOLE) assetic:dump
DUMP_ROUTE       = $(CONSOLE) fos:js-routing:dump
DUMP_TRANSLATION = $(CONSOLE) bazinga:js-translation:dump
MIGRATION        = $(CONSOLE) doctrine:migration:migrate --no-interaction

default: help

help:
	@echo "db          - Install database migrations"
	@echo "dev         - Install for dev"
	@echo "prod        - Install for production"
	@echo "assets-dev  - Install assets for dev"
	@echo "assets-prod - Install assets for production"

db:
	$(MIGRATION)

dev:
	$(COMPOSER_UPDATE)
	$(BOWER)
	$(MAKE) assets-dev

prod:
	$(COMPOSER)
	$(BOWER)
	$(MAKE) assets-prod

assets-dev:
	$(CC)
	$(DUMP_ROUTE)
	$(DUMP_TRANSLATION)
	$(ASSETS)
	$(ASSETIC)

assets-prod:
	$(CC) --env=prod
	$(DUMP_ROUTE) --env=prod
	$(DUMP_TRANSLATION) --env=prod
	$(ASSETS)
	$(ASSETIC) --env=prod